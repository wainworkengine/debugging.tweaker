# Module of WainWorkEngine

No additional dependencies

## Getting started

Add this to your Packages\manifest.json
```
"com.wainwork.engine.debugging.tweaker": "https://gitlab.com/wainworkengine/debugging.tweaker.git#1.0.0",
```


## How to use

### Open window

Open window:
	Tools -> Tweaker

Window has search field.
In upper space showed favorites tweaks.
To add to favorite press "+" button near target tweak.
To remove from favorite press "x" button near target twek in favorite or usual list.

### Tweaks types
Tweaks has manu types.

#### ButtonComponent
Usual tweak, work like a default button.

#### ToggleComponent
Work like button, but has a saved state

#### ValueComponent
Has a value. Value may once of this types:
- float;
- long;
- ulong;
- int;
- string;                                 

### Add runtime tweaks
Create a same class wich inherite of Tweaks class.

```
public class LocalTweaks : Tweaks
{
    protected override IEnumerable<ITweakComponent> GetTweaks()
    {
        return new ITweakComponent[]
        {
            new ButtonComponent
            {
                Caption = "Save Game",
                Action = () => Debug.Log("Save game"),
            },
        };
    }
}
```


### Add editor tweaks
```
[InitializeOnLoad]
public static class EditorTweaks
{
    static EditorTweaks()
    {
        var tweaks = new ITweakComponent[]
        {
            new ToggleComponent
            {
                Caption = "Freeze time",
                Action = value =>
                {
                    Time.timeScale = value ? 0 : 1;
                    return value;
                }
            },
            new ValueComponent<float>
            {
                Caption = "Set time scale",
                Action = value =>
                {
                    Time.timeScale = Mathf.Clamp(value, 0, 100);
                    return value;
                }
            },
        };

        foreach (var tweak in tweaks)
            TTweaker.Add(tweak);
    }
}
```